package ito.poo.clase;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class Persistencia {

	public static void grabaDatos(ArrayList<Cuenta> l) throws FileNotFoundException, IOException {
		ObjectOutputStream file;
		file = new ObjectOutputStream(new FileOutputStream("datos.dat"));
		for(Cuenta cu: l) 
			file.writeObject(cu);
		file.close();
		
	}
	
	public static ArrayList<Cuenta> leeDatos() throws ClassNotFoundException, IOException{
		ArrayList<Cuenta> l = new ArrayList<Cuenta>();
		ObjectInputStream file;
		file= new ObjectInputStream(new FileInputStream("datos.dat"));
		Cuenta cu;
		try {
		    while((cu=(Cuenta)file.readObject())!=null) {
			    l.add(cu);
		     }
		}catch(Exception e) {
			
		}
		file.close();
		return l;
	}

}
