package ito.poo.clase;
import java.time.LocalDate;;

public class Cuenta  implements Comparable<Cuenta> {

	private int numeroDC;
	private String cliente;
	private LocalDate fechaApertura;
	private float saldo;
	private LocalDate fechaUltAct;
	
	public Cuenta(int numeroDC, String cliente, LocalDate fechaApertura, float saldo, LocalDate fechaUltAct){
		super();
		this.numeroDC=numeroDC;
		this.cliente=cliente;
		this.fechaApertura=fechaApertura;
		this.saldo=saldo;
		this.fechaUltAct=fechaUltAct;
	}

	public int getNumeroDC() {
		return numeroDC;
	}

	public void setNumeroDC(int numeroDC) {
		this.numeroDC = numeroDC;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public LocalDate getFechaApertura() {
		return fechaApertura;
	}

	public void setFechaApertura(LocalDate fechaApertura) {
		this.fechaApertura = fechaApertura;
	}

	public float getSaldo() {
		return saldo;
	}

	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}

	public LocalDate getFechaUltAct() {
		return fechaUltAct;
	}

	public void setFechaUltAct(LocalDate fechaUltAct) {
		this.fechaUltAct = fechaUltAct;
	}

	@Override
	public String toString() {
		return "Cuenta [numeroDC=" + numeroDC + ", cliente=" + cliente + ", fechaApertura=" + fechaApertura + ", saldo="
				+ saldo + ", fechaUltAct=" + fechaUltAct + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		result = prime * result + ((fechaApertura == null) ? 0 : fechaApertura.hashCode());
		result = prime * result + ((fechaUltAct == null) ? 0 : fechaUltAct.hashCode());
		result = prime * result + numeroDC;
		result = prime * result + Float.floatToIntBits(saldo);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cuenta other = (Cuenta) obj;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		if (fechaApertura == null) {
			if (other.fechaApertura != null)
				return false;
		} else if (!fechaApertura.equals(other.fechaApertura))
			return false;
		if (fechaUltAct == null) {
			if (other.fechaUltAct != null)
				return false;
		} else if (!fechaUltAct.equals(other.fechaUltAct))
			return false;
		if (numeroDC != other.numeroDC)
			return false;
		if (Float.floatToIntBits(saldo) != Float.floatToIntBits(other.saldo))
			return false;
		return true;
	}

	@Override
	public int compareTo(Cuenta c) {
		// TODO Auto-generated method stub
		if(this.numeroDC!=c.getNumeroDC())
			return this.numeroDC-c.getNumeroDC();
		else if (this.cliente.compareTo(c.getCliente())!=0)
			return this.cliente.compareTo(c.getCliente());
		else if(this.fechaApertura.equals(c.getFechaApertura()))
			return this.fechaApertura.compareTo(c.getFechaApertura());
		else if(this.saldo!=c.getSaldo())
			return this.saldo>c.getSaldo()?1:-1;
		else if(this.fechaUltAct.equals(c.fechaUltAct))
			return this.fechaUltAct.compareTo(c.fechaUltAct);
		return 0;
	}
	
	public void deposito (int cantidad) {
		saldo=saldo+cantidad;
		System.out.println("El total de la cuenta es: "+saldo);
		System.out.println("Fecha del movimiento realizado: "+LocalDate.now());
	}
	
	public boolean retiro(int cantidad) {
		if (saldo>=cantidad);
		saldo=saldo-cantidad;
		System.out.println("El total de la cuenta es: "+saldo);
		System.out.println("Fecha del movimiento realizado: "+LocalDate.now());
		return false;
		
	}

}
